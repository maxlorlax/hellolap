#include "monocypher.h"
#include <err.h>
#include <stdio.h>
#include <unistd.h>
int
main(void)
{
	FILE *f = NULL;
	unsigned char buf[32];

	if (getentropy(buf, 32) != 0)
		err(1, "getentropy");

	f = fopen("x_sk", "wb");
	(void)fwrite(buf, 1, 32, f);
	(void)fclose(f);

	crypto_key_exchange_public_key(buf, buf);
	f = fopen("x_pk", "wb");
	(void)fwrite(buf, 1, 32, f);
	(void)fclose(f);

	if (getentropy(buf, 32) != 0)
		err(1, "getentropy");

	f = fopen("s_sk", "wb");
	(void)fwrite(buf, 1, 32, f);
	(void)fclose(f);

	crypto_ed25519_sign_public_key(buf, buf);
	f = fopen("s_pk", "wb");
	(void)fwrite(buf, 1, 32, f);
	(void)fclose(f);
}
