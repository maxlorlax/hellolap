#!/usr/bin/env python3
import binascii
import hmac
import secrets
import struct
import sys

# We have 110 bits to safely work with.
# Layout:
# The first 88 form a MAC over the next 22 bits.
# The next 22 bits identify the copy (little-endian),
key = secrets.randbelow(2**22)
keyb = key.to_bytes(length=3, byteorder='little')
print("unique identifier: %d (%s)" % (key, binascii.hexlify(keyb).decode('ascii')))

secret = binascii.unhexlify('36d3e1b406ea3254a0a00124d1fc39bd79225e57101648f429c5fed3a5e241f1')

h = hmac.new(secret, keyb, 'sha512')
keyb = h.digest()[:11] + keyb
print(binascii.hexlify(keyb).decode('ascii'))
print(len(keyb))

total = 0
for n in reversed(keyb):
    total <<= 8
    total |= n
print("bit len = %d" % (total.bit_length()))

ALPHABET = 'CDFGJKLMQRSTUVWXZ2345679'
divisor = 24**(23) # 23 is one less than the number of output characters
out = ''
n, m = 25, 24
a = m
for i in range(24):
    pos, total = total // divisor, total % divisor
    divisor //= len(ALPHABET)
    if i > 0 and i % 5 == 0:
        out += '-'
    out += ALPHABET[pos]

    a = (pos + a) % m
    if a == 0: a = m
    a *= 2
    a %= n
if divisor != 0:
    print("BAD DIVISOR: %d" % divisor)
    sys.exit(1)

def try_decode(enc):
    # b is a big integer array, 8-bit limbs
    b = [0]*14
    for ch in enc:
        if ch == '-': continue
        # multiply by base, in this case 24
        w = [0] * (len(b) + 1)
        #for i in range(len(b)):
        #    c = 0
        #    x = w[i] + 24 * b[i] + c
        #    w[i] = x & 0xff
        #    c = x >> 8
        #
        #    for j in range(1, len(b) - 1):
        #        x = w[i + j] + c
        #        w[i + j] = x & 0xff
        #        c = x >> 8
        #
        #    w[i + 2] = c
        for b_i in range(len(b)):
            x = w[b_i] + 24 * b[b_i]
            c = x >> 8
            w[b_i] = x & 0xff
            w[b_i + 1] = c

        b = w[0:14] # no overflow with inputs of expected length
        print(''.join(['%02x'%z for z in reversed(b)]))

        # add digit value in alphabet
        c = 0
        print("+ 0x%02x" % ALPHABET.index(ch))
        x = b[0] + ALPHABET.index(ch)
        c = 1 if x > 0xff else 0
        b[0] = x & 0xff
        for i in range(1, len(b)):
            x = b[i] + c
            c = 1 if x > 0xff else 0
            b[i] = x & 0xff
    ret = bytes(b)
    print(ret)
    return ret

if try_decode(out) != keyb:
    print(keyb)
    sys.exit(1)

out += ALPHABET[(m + 1 - (a % m)) % m]
print(out)

