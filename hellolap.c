#include "stdafx.h"

////////////////////////////////////////////////////////////////////////////////
//
//  Various lengths
//
////////////////////////////////////////////////////////////////////////////////
#define POLY1305_TAG_LENGTH        16
#define CHACHA20_KEY_LENGTH        32
#define POLY1305_KEY_LENGTH        32
#define ED25519_PUBLIC_KEY_LENGTH  32
#define X25519_SECRET_KEY_LENGTH   32
#define X25519_PUBLIC_KEY_LENGTH   32
#define X25519_RESULT_LENGTH       32
#define ED25519_SIGNATURE_LENGTH   64

#define PROTOCOL_VERSION           2

// § 3.3
#define CLIENT_HEADER_LENGTH       (1+2+5+16+16+16+16+16)
// § 3.4
#define SERVER_HEADER_LENGTH       (1+2+5+16+16+16)

// These could also be dynamic by spec, but in this example, they're hard-coded
#define SERVER_DATA_LENGTH         14
#define CLIENT_SEED_LENGTH         14

// Server total size: ED25519_PUBLIC_KEY_LENGTH + SERVER_HEADER_LENGTH + SERVER_DATA_LENGTH + POLY1305_TAG_LENGTH = 64 + 56 + 14 + 16 = 150
#define SERVER_PACKET_LENGTH          (ED25519_SIGNATURE_LENGTH + SERVER_HEADER_LENGTH + SERVER_DATA_LENGTH + POLY1305_TAG_LENGTH)
// Client unpadded size: X25519_PUBLIC_KEY_LENGTH + CLIENT_HEADER_LENGTH + CLIENT_SEED_LENGTH + POLY1305_TAG_LENGTH = 32 + 56 + 14 + 16 + 16 + 16 = 150
#define CLIENT_UNPADDED_PACKET_LENGTH (X25519_PUBLIC_KEY_LENGTH + CLIENT_HEADER_LENGTH + CLIENT_SEED_LENGTH + POLY1305_TAG_LENGTH)
// To avoid network amplification attacks, a client packet MUST be
// at least as large as a server packet.
// This code pads the client seed if necessary.
#if CLIENT_UNPADDED_PACKET_LENGTH < SERVER_PACKET_LENGTH
#define CLIENT_DATA_LENGTH         (CLIENT_SEED_LENGTH + (SERVER_PACKET_LENGTH - CLIENT_UNPADDED_PACKET_LENGTH))
#define CLIENT_PACKET_LENGTH       (CLIENT_UNPADDED_PACKET_LENGTH + (SERVER_PACKET_LENGTH - CLIENT_UNPADDED_PACKET_LENGTH))
#else
#define CLIENT_PACKET_LENGTH       (CLIENT_UNPADDED_PACKET_LENGTH)
#define CLIENT_DATA_LENGTH         (CLIENT_SEED_LENGTH)
#endif
static_assert(CLIENT_PACKET_LENGTH <= SERVER_PACKET_LENGTH,
		"client packet length too short");

////////////////////////////////////////////////////////////////////////////////
//
//  UUID functions
//
////////////////////////////////////////////////////////////////////////////////
STATIC UUID CreateUUID(INT version)
{
	UUID uuid = {0};
	switch (version)
	{
	case 0: break; // nil UUID

	case 4: {
		if (getentropy(&uuid, sizeof(uuid)) != 0)
			err(1, "getentropy for UUID4 failed");
		uuid.ClockSeqHighAndReserved &= 0x3F;
		uuid.ClockSeqHighAndReserved |= 0x80;
		uuid.TimeHighAndVersion &= 0x0FFF;
		uuid.TimeHighAndVersion |= 0x4000;
		}
		break;
	default:
		errx(1, "unsupported client UUID method");
	}
	return uuid;
}

STATIC VOID PrintUUID(UUID uuid)
{
	printf("%08" PRIx32 "-%04" PRIx16
			"-%04" PRIx16
			"-%02" PRIx8 "%02" PRIx8
			"-%02" PRIx8 "%02" PRIx8 "%02" PRIx8 "%02" PRIx8 "%02" PRIx8 "%02" PRIx8,
			uuid.TimeLow, uuid.TimeMid, uuid.TimeHighAndVersion,
			uuid.ClockSeqHighAndReserved, uuid.ClockSeqLow,
			uuid.Node[0], uuid.Node[1], uuid.Node[2],
			uuid.Node[3], uuid.Node[4], uuid.Node[5]);
}

STATIC VOID StoreUUID(BYTE out[static 16], UUID uuid)
{
	// RFC 4122 § 4.1.2, in its infinite wisdom, mandated big-endian
	// representation of UUIDs.
	out[0] = (BYTE)((uuid.TimeLow >> 24) & 0xff);
	out[1] = (BYTE)((uuid.TimeLow >> 16) & 0xff);
	out[2] = (BYTE)((uuid.TimeLow >>  8) & 0xff);
	out[3] = (BYTE)( uuid.TimeLow        & 0xff);

	out[4] = (BYTE)((uuid.TimeMid >>  8) & 0xff);
	out[5] = (BYTE)( uuid.TimeMid        & 0xff);

	out[6] = (BYTE)((uuid.TimeHighAndVersion >>  8) & 0xff);
	out[7] = (BYTE)( uuid.TimeHighAndVersion        & 0xff);

	out[8] = uuid.ClockSeqHighAndReserved;
	out[9] = uuid.ClockSeqLow;

	memcpy(&out[10], uuid.Node, 6);
}

STATIC VOID LoadUUID(UUID *uuid, CONST BYTE *in)
{
	uuid->TimeLow = ((DWORD)in[0] << 24) |
		((DWORD)in[1] << 16) |
		((DWORD)in[2] << 8) |
		(DWORD)in[3];
	uuid->TimeMid = (WORD)(((WORD)in[4] << 8) |
		(WORD)in[5]);
	uuid->TimeHighAndVersion = (WORD)(((WORD)in[6] << 8) |
		(WORD)in[7]);
	uuid->ClockSeqHighAndReserved = in[8];
	uuid->ClockSeqLow = in[9];
	memcpy(uuid->Node, &in[10], 6);
}

////////////////////////////////////////////////////////////////////////////////
//
//  License key handling functions
//
////////////////////////////////////////////////////////////////////////////////
STATIC INT LicenseKeyCharacterToInteger(INT ch)
{
	switch (ch)
	{
	case 'C':
		return 0;
	case 'D':
		return 1;
	case 'F':
		return 2;
	case 'G':
		return 3;
	case 'J':
		return 4;
	case 'K':
		return 5;
	case 'L':
		return 6;
	case 'M':
		return 7;
	case 'Q':
		return 8;
	case 'R':
		return 9;
	case 'S':
		return 10;
	case 'T':
		return 11;
	case 'U':
		return 12;
	case 'V':
		return 13;
	case 'W':
		return 14;
	case 'X':
		return 15;
	case 'Z':
		return 16;
	case '2':
		return 17;
	case '3':
		return 18;
	case '4':
		return 19;
	case '5':
		return 20;
	case '6':
		return 21;
	case '7':
		return 22;
	case '9':
		return 23;
	default:
		errx(1, "Bad license key character %d", ch);
	}
}

// Algorithm follows ISO 7064:2003 for a 24-character hybrid system MOD 25,24.
STATIC BOOL IsValidLicenseKeyString(LPCSTR szLicenseKey)
{
	CONST INT n = 25, m = 24;
	INT total = 0;
	INT check = 0;
	CHAR ch;
	if (strlen(szLicenseKey) != 29 ||
			szLicenseKey[5] != '-' ||
			szLicenseKey[11] != '-' ||
			szLicenseKey[17] != '-' ||
			szLicenseKey[23] != '-')
		return FALSE;
	while ((ch = *szLicenseKey++) != '\0')
	{
		if (ch == '-')
			continue;
		check = (total % n) + LicenseKeyCharacterToInteger(ch);
		total = check % m;
		if (total == 0)
			total = m;
		total <<= 1;
	}

	return (check % m) == 1;
}

STATIC VOID LicenseKeyStringToByteArray(LPCSTR szLicenseKey,
		BYTE arrbLicenseKey[STATIC CLIENT_SEED_LENGTH])
{
	SIZE i, j;
	INT ch, x, c;
	BYTE w[15];
	// ignore last character because it's a meaningless check digit
	for (i = 0; i < 28; ++i)
	{
		if ((ch = szLicenseKey[i]) == '-')
			continue;

		// Multiply the buffer by the base first, in this case 24
		memset(w, 0, sizeof(w));

		for (j = 0; j < CLIENT_SEED_LENGTH; ++j)
		{
			x = w[j] + 24 * arrbLicenseKey[j];
			c = x >> 8;
			w[j] = (BYTE)(x & 0xff);
			w[j + 1] = (BYTE)(c);
		}

		memcpy(arrbLicenseKey, w, CLIENT_SEED_LENGTH);

		// add digit value in alphabet
		c = 0;
		x = arrbLicenseKey[0] + LicenseKeyCharacterToInteger(ch);
		c = (x > 0xff);
		arrbLicenseKey[0] = (BYTE)(x & 0xff);
		for (j = 1; j < CLIENT_SEED_LENGTH; ++j)
		{
			x = arrbLicenseKey[j] + c;
			c = (x > 0xff);
			arrbLicenseKey[j] = (BYTE)(x & 0xff);
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
//
//  Provisioning (§ 3.1)
//
////////////////////////////////////////////////////////////////////////////////
static struct {
	BYTE ServerKeyExchangePublicKey[X25519_PUBLIC_KEY_LENGTH];
	BYTE ServerSignaturePublicKey[ED25519_PUBLIC_KEY_LENGTH];
	UUID SKUId;
	UUID BaseId;
	BYTE Seed[CLIENT_DATA_LENGTH];
} Client = {
	.ServerKeyExchangePublicKey = {
	0x6f, 0x87, 0x7b, 0x69, 0x0c, 0x19, 0xec, 0x4b, 0x51, 0xe8, 0xf9, 0xc9,
	0x4f, 0xe9, 0x5e, 0x5b, 0x8f, 0xe5, 0x68, 0x78, 0xa2, 0x16, 0xe3, 0x0d,
	0xd1, 0x20, 0x27, 0x92, 0xd1, 0xb4, 0x59, 0x30
	},
	.ServerSignaturePublicKey = {
	0x91, 0x91, 0x3c, 0x85, 0x4f, 0x92, 0xa8, 0x36, 0xe2, 0xd7, 0x3e, 0x98,
	0x71, 0x1e, 0x71, 0x2f, 0xcc, 0x41, 0x9f, 0x24, 0x0c, 0x50, 0xaf, 0x60,
	0x50, 0xea, 0x6e, 0x2b, 0x3d, 0x32, 0x6a, 0xc9
	},
	.SKUId = { // afde6e72-3b0e-413d-8f4d-345fb84a7976
		0xafde6e72,
		0x3b0e,
		0x413d,
		0x8f, 0x4d,
		{0x34, 0x5f, 0xb8, 0x4a, 0x79, 0x76}
	},
};

STATIC VOID Provision(LPCSTR szLicenseKey)
{
	// You would normally ask a user for some kind of licensing information
	// at this point, such as a license file or a string.
	// For the sake of illustration, this is currently hard-coded.
	BYTE arrbLicenseKey[CLIENT_SEED_LENGTH] = {0};

	if (!IsValidLicenseKeyString(szLicenseKey))
		errx(1, "Invalid license key string given");
	LicenseKeyStringToByteArray(szLicenseKey, arrbLicenseKey);
	printf("Client seed (decoded): ");
	for (SIZE i = 0; i < CLIENT_SEED_LENGTH; ++i)
		printf("%02" PRIx8 " ", arrbLicenseKey[i]);

	memset(Client.Seed, 0, sizeof(Client.Seed));
	memcpy(Client.Seed, arrbLicenseKey, CLIENT_SEED_LENGTH);

	printf("\nSKU: ");
	PrintUUID(Client.SKUId);
	printf("\nClient UUID: ");
	Client.BaseId = CreateUUID(4);
	PrintUUID(Client.BaseId);
	puts("");
}

////////////////////////////////////////////////////////////////////////////////
//
//  Cryptography helpers
//
////////////////////////////////////////////////////////////////////////////////
STATIC VOID DeriveKeys(LPBYTE esk, LPBYTE epk, LPBYTE clisk, LPBYTE srvsk)
{
	STATIC CONST BYTE salt[64]; // RFC 5869 § 2.2: no salt = [0]*HashLen
	BYTE shared[32];
	BYTE prk[64];
	crypto_hmac_sha512_ctx ctx[1];

	crypto_x25519(shared, esk, Client.ServerKeyExchangePublicKey);
	crypto_wipe(esk, X25519_SECRET_KEY_LENGTH);

	// PRK=HKDF-Extract(salt=NULL, epk || srvxpk || srvspk || X25519-shared)
	{
		crypto_hmac_sha512_init(ctx, salt, sizeof(salt));
		crypto_hmac_sha512_update(ctx, epk, X25519_PUBLIC_KEY_LENGTH);
		crypto_hmac_sha512_update(ctx,
				Client.ServerKeyExchangePublicKey, X25519_PUBLIC_KEY_LENGTH);
		crypto_hmac_sha512_update(ctx,
				Client.ServerSignaturePublicKey, ED25519_PUBLIC_KEY_LENGTH);
		crypto_hmac_sha512_update(ctx, shared, X25519_RESULT_LENGTH);
		crypto_wipe(shared, sizeof(shared));
		crypto_hmac_sha512_final(ctx, prk);
		crypto_wipe(ctx, sizeof(*ctx));
	}

	// OKM=HKDF-Expand(PRK, info, L=64)
	// We can simplify a lot since we only really do a singular invocation
	// of the hash function.
	{
		STATIC CONST BYTE info[] =
			"56065c4d-d2e0-4ba9-bf9f-76f9159e2987-LAP-V02";
		BYTE okm[64];

		crypto_hmac_sha512_init(ctx, prk, sizeof(prk));
		crypto_wipe(prk, sizeof(prk));
		// T(0) is empty string with zero length
		crypto_hmac_sha512_update(ctx, info, 44);
		crypto_hmac_sha512_update(ctx, &(BYTE){0x01}, 1);
		crypto_hmac_sha512_final(ctx, okm);
		crypto_wipe(ctx, sizeof(*ctx));

		memcpy(clisk, okm, CHACHA20_KEY_LENGTH);
		memcpy(srvsk, okm + CHACHA20_KEY_LENGTH, CHACHA20_KEY_LENGTH);
		crypto_wipe(okm, sizeof(okm));
	}
}

STATIC VOID Store64LE(LPBYTE buf, QWORD n)
{
	buf[0]  = (BYTE)(n & 0xff);
	buf[1]  = (BYTE)((n >> 8) & 0xff);
	buf[2] = (BYTE)((n >> 16) & 0xff);
	buf[3] = (BYTE)((n >> 24) & 0xff);
	buf[4] = (BYTE)((n >> 32) & 0xff);
	buf[5] = (BYTE)((n >> 40) & 0xff);
	buf[6] = (BYTE)((n >> 48) & 0xff);
	buf[7] = (BYTE)((n >> 56) & 0xff);
}

STATIC VOID ChaPolyEncrypt(LPBYTE sk, LPBYTE buf, SIZE buflen, LPBYTE tag)
{
	STATIC CONST BYTE nonce[12] = {0}; // § 3.3, fixed nonce
	crypto_poly1305_ctx ctx[1];
	BYTE poly1305_key[POLY1305_KEY_LENGTH];
	BYTE zero[16] = {0};

	// Generate Poly1305 one-time key
	crypto_ietf_chacha20_ctr(poly1305_key, NULL,
			sizeof(poly1305_key), sk, nonce, 0);
	// Perform in-place ChaCha20 encryption now
	crypto_ietf_chacha20_ctr(buf, buf, buflen, sk, nonce, 1);
	crypto_wipe(sk, CHACHA20_KEY_LENGTH);
	crypto_poly1305_init(ctx, poly1305_key);
	crypto_poly1305_update(ctx, buf, buflen);
	// RFC 8439 § 2.8, align to multiple of 16 bytes and store
	// sizes of AAD (none => 0) and ciphertext as 64-bit LE ints
	crypto_poly1305_update(ctx, zero, ((SIZE)-buflen) & 0xf);
	// Reuse "zero" as buffer for storing the sizes.
	Store64LE(&zero[8], buflen);
	crypto_poly1305_update(ctx, zero, 16);
	crypto_poly1305_final(ctx, tag); // wipes ctx implicitly
	crypto_wipe(poly1305_key, sizeof(poly1305_key));
}

STATIC BOOL ChaPolyDecrypt(LPBYTE sk, LPBYTE buf, SIZE buflen, LPBYTE tag)
{
	STATIC CONST BYTE nonce[12] = {0}; // § 3.4, fixed nonce
	crypto_poly1305_ctx ctx[1];
	BYTE poly1305_key[POLY1305_KEY_LENGTH];
	BYTE zero[16] = {0};

	// Generate Poly1305 one-time key
	crypto_ietf_chacha20_ctr(poly1305_key, NULL,
			sizeof(poly1305_key), sk, nonce, 0);
	crypto_poly1305_init(ctx, poly1305_key);
	crypto_wipe(poly1305_key, sizeof(poly1305_key));
	crypto_poly1305_update(ctx, buf, buflen);
	// RFC 8439 § 2.8, align to multiple of 16 bytes and store
	// sizes of AAD (none => 0) and ciphertext as 64-bit LE ints
	crypto_poly1305_update(ctx, zero, ((SIZE)-buflen) & 0xf);
	// Reuse "zero" as buffer for storing the sizes.
	Store64LE(&zero[8], buflen);
	crypto_poly1305_update(ctx, zero, 16);
	// now reuse zero as buffer for the tag
	crypto_poly1305_final(ctx, zero); // wipes ctx implicitly

	if (crypto_verify16(tag, zero) != 0)
	{
		crypto_wipe(sk, CHACHA20_KEY_LENGTH);
		return FALSE;
	}
	// Perform in-place ChaCha20 encryption now
	crypto_ietf_chacha20_ctr(buf, buf, buflen, sk, nonce, 1);
	crypto_wipe(sk, CHACHA20_KEY_LENGTH);
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
//
//  Activation + DNS stuff (§ 3.2 and 3.3)
//
////////////////////////////////////////////////////////////////////////////////
typedef struct {
	struct ares_srv_reply   *AresRawQueryTargets;   // must be freed
	struct ares_srv_reply  **QueryTargets;          // unusable after ^
	LPCSTR                   Error;                 // must NOT be freed
	struct sockaddr_storage  TargetSocketAddr;
	socklen_t                TargetSocketAddrLength;
	INT                      TargetSocketFamily;
	INT                      TargetSocketType;
	INT                      TargetSocketProtocol;
	DWORD                    dwQueryTargets;
	WORD                     TargetPort;
	BOOL                     Success;        // TODO convert to HRESULT
} QUERYSTATUS, *LPQUERYSTATUS;

// -2 to indicate socket error; everything else is protocol-level failure
STATIC INT Activate(LPQUERYSTATUS qs)
{
	// ephemeral X25519 secret key;
	// public key goes in arrbPacket directly
	BYTE esk[X25519_PUBLIC_KEY_LENGTH];
	// symmetric keys for client and server, respectively
	BYTE clisk[CHACHA20_KEY_LENGTH], srvsk[CHACHA20_KEY_LENGTH];
	BYTE arrbPacket[X25519_PUBLIC_KEY_LENGTH + 
		CLIENT_HEADER_LENGTH + sizeof(Client.Seed) +
		POLY1305_TAG_LENGTH];
	LPBYTE epk          = arrbPacket;
	LPBYTE arrbRequest  = arrbPacket + X25519_PUBLIC_KEY_LENGTH ;
	SIZE   sRequestSize = CLIENT_HEADER_LENGTH + sizeof(Client.Seed);
	LPBYTE tag          = arrbPacket + X25519_PUBLIC_KEY_LENGTH + sRequestSize;
	arrbRequest[0] = PROTOCOL_VERSION;
	arrbRequest[1] = (BYTE)sRequestSize;
	arrbRequest[2] = 0;
	{
		// time_t may be signed 32-bit, so the >> 32 may be undefined,
		// so use int64_t instead
		int64_t now = (int64_t)time(NULL);

		arrbRequest[3] = (BYTE)(now & 0xff);
		arrbRequest[4] = (BYTE)((now >> 8) & 0xff);
		arrbRequest[5] = (BYTE)((now >> 16) & 0xff);
		arrbRequest[6] = (BYTE)((now >> 24) & 0xff);
		arrbRequest[7] = (BYTE)((now >> 32) & 0xff);
	}

	StoreUUID(&arrbRequest[8], Client.BaseId);

	{
		UUID ClientAddOnId;
		ClientAddOnId = CreateUUID(0);
		printf("Add-on ID: ");
		PrintUUID(ClientAddOnId);
		puts("");
		StoreUUID(&arrbRequest[24], ClientAddOnId);
	}

	StoreUUID(&arrbRequest[40], Client.SKUId);

	{ // This would need to be read from some kind of storage if not first query
		UUID CurrentLicenseId;
		CurrentLicenseId = CreateUUID(0);
		printf("Current license ID: ");
		PrintUUID(CurrentLicenseId);
		puts("");
		StoreUUID(&arrbRequest[56], CurrentLicenseId);
	}

	// reserved field is all-zero
	memset(&arrbRequest[72], 0, 16);

	memcpy(&arrbRequest[CLIENT_HEADER_LENGTH],
			Client.Seed, sizeof(Client.Seed));

	if (getentropy(esk, sizeof(esk)) != 0)
	{
		qs->Error = "getentropy for ephemeral X25519 key failed";
		return -1;
	}
	crypto_x25519_public_key(epk, esk);
	DeriveKeys(esk, epk, clisk, srvsk); // wipes esk

	ChaPolyEncrypt(clisk, arrbRequest, sRequestSize, tag); // wipes clisk

	INT fd;

	if ((fd = socket(qs->TargetSocketFamily, qs->TargetSocketType,
			qs->TargetSocketProtocol)) == -1)
	{
		qs->Error = strerror(errno);
		return -2;
	}

	SSIZE cbSent = sendto(fd, arrbPacket, sizeof(arrbPacket),
			0,
			(const struct sockaddr *)&qs->TargetSocketAddr,
			qs->TargetSocketAddrLength);
	// Must send everything as one UDP packet.
	if (cbSent == -1 || (SIZE)cbSent != sizeof(arrbPacket))
	{
		qs->Error = strerror(errno);
		(void)close(fd);
		return -2;
	}

	LPBYTE arrbSignature = arrbPacket;
	LPBYTE arrbResponse  = arrbPacket + ED25519_SIGNATURE_LENGTH;
	SIZE   sResponseSize = SERVER_HEADER_LENGTH + SERVER_DATA_LENGTH;
	tag                  = arrbPacket + ED25519_SIGNATURE_LENGTH + sResponseSize;

	{
		struct timeval tv = {10, 0};
		// A ten-second timeout ought to be reasonable even on the
		// worst of networks.
		if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv))
				!= 0)
		{
			qs->Error = "cannot set socket timeout";
			return -2;
		}
	}


	SSIZE cbRead = recvfrom(fd, arrbPacket, sizeof(arrbPacket), MSG_WAITALL,
			(struct sockaddr *)&qs->TargetSocketAddr,
			&qs->TargetSocketAddrLength);
	(void)close(fd);

	if (cbRead == -1 || (SIZE)cbRead != sizeof(arrbPacket))
	{
		qs->Error = strerror(errno);
		return -2;
	}

	if (crypto_ed25519_check(arrbSignature, Client.ServerSignaturePublicKey,
				arrbResponse, sResponseSize + POLY1305_TAG_LENGTH) != 0)
	{
		qs->Error = "bad signature over response";
		return -1;
	}

	// ChaPolyDecrypt wipes srvsk
	if (!ChaPolyDecrypt(srvsk, arrbResponse, sResponseSize, tag))
	{
		qs->Error = "bad Poly1305 tag from server";
		return -1;
	}

	if (arrbResponse[0] != PROTOCOL_VERSION)
	{
		qs->Error = "bad response version";
		return -1;
	}
	if (((SIZE)arrbResponse[1] | ((SIZE)arrbResponse[2] << 8))
			!= sResponseSize)
	{
		qs->Error = "bad response size";
		return -1;
	}

	{
		// int64_t because time_t may be 32-bit
		int64_t theirs = (int64_t)arrbResponse[3] |
			((int64_t)arrbResponse[4] << 8) |
			((int64_t)arrbResponse[5] << 16) |
			((int64_t)arrbResponse[6] << 24) |
			((int64_t)arrbResponse[7] << 32);
		int64_t now = (int64_t)time(NULL);
		int64_t delta = now - theirs;

		if (delta < 0)
			delta = -delta;

		if (delta > 30)
		{
			printf("mine = %" PRId64 ", theirs = %" PRId64 ", delta = %" PRId64"\n", now, theirs, delta);
			qs->Error = "time delta too large";
			return -1;
		}
	}

	{
		UUID clientId, skuId, licenseId;
		LoadUUID(&clientId, &arrbResponse[8]);
		LoadUUID(&skuId, &arrbResponse[8+16]);
		LoadUUID(&licenseId, &arrbResponse[8+16+16]);

		if (memcmp(&clientId, &Client.BaseId, 16) != 0)
		{
			qs->Error = "client ID mismatch";
			return -1;
		}
		if (memcmp(&skuId, &Client.SKUId, 16) != 0)
		{
			qs->Error = "SKU ID mismatch";
			return -1;
		}

		printf("Received license ID ");
		PrintUUID(licenseId);
	}

	{
		CHAR message[SERVER_DATA_LENGTH + 1];
		memcpy(message, &arrbResponse[SERVER_HEADER_LENGTH],
				SERVER_DATA_LENGTH);
		message[SERVER_DATA_LENGTH] = '\0';
		printf(" with message: %s", message);
	}

	return 0;
}

STATIC INT ProcessAres(ares_channel chan, LPQUERYSTATUS qs)
{
	while (TRUE) {
		struct timeval *tva, tv;
		fd_set fsReadFDs, fsWriteFDs;
		INT iFDCount, errno_bak;

		FD_ZERO(&fsReadFDs);
		FD_ZERO(&fsWriteFDs);
		iFDCount = ares_fds(chan, &fsReadFDs, &fsWriteFDs);
		if (iFDCount == 0)
			break;
		tva = ares_timeout(chan, NULL, &tv);
		if (select(iFDCount, &fsReadFDs, &fsWriteFDs, NULL, tva) == -1)
		{
			errno_bak = errno;
			ares_destroy(chan);
			if (qs->QueryTargets != NULL)
				ares_free_data(qs->AresRawQueryTargets);
			return errno_bak;
		}
		ares_process(chan, &fsReadFDs, &fsWriteFDs);
	}
	return 0;
}

// The algorithm in RFC 2782 is only a "SHOULD".
// We deviate from the algorithm to create a list of servers to contact in
// one shot, sorting absolutely by priority first, weight second.
// This sort of defeats the point of the weight, but it's practical.
STATIC INT SRVRecordComparison(CONST VOID *a, CONST VOID *b)
{
	const struct ares_srv_reply *ra = (const struct ares_srv_reply *)a;
	const struct ares_srv_reply *rb = (const struct ares_srv_reply *)b;

	// lowest priority first
	if (ra->priority == rb->priority)
	{
		if (ra->weight == rb->weight)
			return 0;
		else if (ra->weight > rb->weight) // inverse order from priority
			return 1;
		else
			return -1;
	}
	else if (rb->priority > ra->priority)
		return 1;
	else
		return -1;
}

STATIC VOID DiscoverServiceDNSCallback(LPVOID arg, INT status, INT timeouts,
		LPUCHAR arrucResponse, INT iResponseLength)
{
	LPQUERYSTATUS qs = (LPQUERYSTATUS)arg;
	DWORD dwResponses = 0;
	struct ares_srv_reply *asrReplies;

	(void)timeouts;
	qs->Success = FALSE;
	if (status != ARES_SUCCESS)
	{
		qs->Error = ares_strerror(status);
		return;
	}

	if ((status = ares_parse_srv_reply(arrucResponse, iResponseLength,
				&asrReplies)) != ARES_SUCCESS)
	{
		qs->Error = ares_strerror(status);
		return;
	}

	if (asrReplies == NULL)
	{
		qs->Error = "empty response to SRV query";
		return;
	}

	// RFC 2782 p. 6 et seq.
	for (struct ares_srv_reply *r = asrReplies;
			r != NULL;
			r = r->next)
		++dwResponses;
	// If there is exactly one response and it is ".", give up.
	if (dwResponses == 1 && strcmp(asrReplies->host, ".") == 0)
	{
		ares_free_data(asrReplies);
		qs->Error = "precisely one SRV RR and Target is '.'";
		return;
	}

	// Create a list of candidates (prio, weight, target)
	// ordered by prio (lowest first), then do the weight-based selection.
	// The algorithm for the weight-based selection is specified in
	// RFC 2782 p. 4 (but just "SHOULD" use that algorithm);
	// for practicality reasons to avoid excess allocations, we do both in
	// the sorting step
	struct ares_srv_reply **rrs = calloc(dwResponses,
			sizeof(struct ares_srv_reply));
	if (rrs == NULL)
	{
		ares_free_data(asrReplies);
		qs->Error = "cannot calloc SRV list";
		return;
	}
	DWORD i = 0;
	for (struct ares_srv_reply *r = asrReplies;
			r != NULL;
			r = r->next)
	{
		rrs[i++] = r;
	}
	qsort(rrs, dwResponses, sizeof(*rrs), SRVRecordComparison);

	// rrs is now sorted by lowest priority then by greatest weight.

	qs->AresRawQueryTargets = asrReplies;
	qs->QueryTargets = rrs;
	qs->dwQueryTargets = dwResponses;
	qs->Success = TRUE;
}

STATIC VOID DiscoverServiceAndActivate(LPQUERYSTATUS qs, ares_channel chan,
		LPCSTR ourDomain)
{
	STATIC LPCSTR srvPrefix = "_lap._udp.";

	SIZE domainLength = strlen(srvPrefix) + strlen(ourDomain) + 2;
	LPSTR domainToQueryForSRV = malloc(domainLength);

	if (domainToQueryForSRV == NULL)
		err(1, "malloc");

	snprintf(domainToQueryForSRV, domainLength, "%s%s.",
			srvPrefix, ourDomain);
	ares_search(chan, domainToQueryForSRV, C_IN, T_SRV,
			DiscoverServiceDNSCallback, qs);
	warnx("Trying SRV query for %s", domainToQueryForSRV);
	if (ProcessAres(chan, qs) != 0)
	{
		free(domainToQueryForSRV);
		err(1, "select");
	}
	free(domainToQueryForSRV);

	if (!qs->Success)
		errx(1, "error performing SRV query: %s", qs->Error);

	qs->Success = FALSE;
	// qs->QueryTargets now has a list of hosts we can try.
	// First one that works is the one we'll use.
	struct addrinfo aiHints = {
		.ai_family = AF_UNSPEC,
		.ai_socktype = SOCK_DGRAM,
		.ai_flags = AI_NUMERICSERV
	};
	struct addrinfo *lpaiResults;
	CHAR szPort[6];
	INT gai_err;
	for (DWORD i = 0; i < qs->dwQueryTargets; ++i)
	{
		snprintf(szPort, sizeof(szPort),
				"%hu", qs->QueryTargets[i]->port);
		if ((gai_err = getaddrinfo(qs->QueryTargets[i]->host, szPort,
					&aiHints, &lpaiResults)) != 0)
		{
			warnx("getaddrinfo after SRV failure: %s",
					gai_strerror(gai_err));
			continue;
		}

		for (struct addrinfo *ai = lpaiResults;
				ai != NULL;
				ai = ai->ai_next)
		{
			memcpy(&qs->TargetSocketAddr,
					ai->ai_addr, ai->ai_addrlen);
			qs->TargetSocketAddrLength = ai->ai_addrlen;
			qs->TargetSocketFamily     = ai->ai_family;
			qs->TargetSocketType       = ai->ai_socktype;
			qs->TargetSocketProtocol   = ai->ai_protocol;
			if (Activate(qs) != -2) // TODO HRESULT
			{
				ares_free_data(qs->AresRawQueryTargets);
				free(qs->QueryTargets);
				if (qs->Error != NULL)
					printf("activation error: %s\n", qs->Error);
				else
					puts("no error");
				freeaddrinfo(lpaiResults);
				return;
			}
		}
	}
	ares_free_data(qs->AresRawQueryTargets);
	free(qs->QueryTargets);

	errx(1, "all attempts (%" PRIu32 ") to find and "
			"contact an activation server failed",
			qs->dwQueryTargets);
}

////////////////////////////////////////////////////////////////////////////////
//
//  main function
//
////////////////////////////////////////////////////////////////////////////////
INT main(INT argc, LPSTR argv[])
{
	ares_channel chan;
	QUERYSTATUS qs;

	// Provisioning is supposed to be done once, during installation;
	// for demonstration purposes, we re-provision on every run
	if (argc < 2)
		errx(1, "usage: %s license-key-here", argv[0]);

	memset(&qs, 0, sizeof(qs));

	Provision(argv[1]);

	if (ares_library_init(ARES_LIB_INIT_NONE) != ARES_SUCCESS)
	{
		errx(1, "ares_library_init failed");
	}
	if (ares_init(&chan) != ARES_SUCCESS)
	{
		errx(1, "cannot initialize c-ares");
	}

	DiscoverServiceAndActivate(&qs, chan, "lorlacks.test");
	ares_destroy(chan);
}

