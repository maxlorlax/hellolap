CFLAGS = -Weverything -Wno-padded -O0 -g
LDLIBS = -lcares

hellolap: hellolap.o monocypher.o monocypher-ed25519.o
	$(CC) -o hellolap hellolap.o monocypher.o monocypher-ed25519.o $(LDLIBS)

