#pragma once

#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h> // struct timeval

#include <assert.h>
#include <err.h>
#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

// DNS library: c-ares
#include <ares.h>
#include <arpa/nameser.h>
#include <netdb.h>

// Cryptography library: Monocypher
#include "monocypher.h"
#include "monocypher-ed25519.h"

// OpenBSD compat hack because arpa/nameser.h never got updated there
#ifndef C_ANY
#define C_ANY ns_c_any
#endif
#ifndef C_IN
#define C_IN ns_c_in
#endif
#ifndef T_SRV
#define T_SRV ns_t_srv
#endif

typedef uint8_t        BYTE;
typedef uint8_t       *LPBYTE;
typedef const uint8_t *LPCBYTE;
typedef uint16_t       WORD;
typedef uint16_t      *LPWORD;
typedef uint32_t       DWORD;
typedef uint32_t      *LPDWORD;
typedef uint64_t       QWORD;
typedef uint64_t      *LPQWORD;
typedef char          *LPSTR;
typedef const char    *LPCSTR;

typedef char           CHAR;
typedef unsigned char  UCHAR;
typedef unsigned char *LPUCHAR;
typedef bool           BOOL;
typedef int            INT;
typedef unsigned int   UINT;
typedef void           VOID;
typedef void          *LPVOID;

typedef size_t         SIZE;
typedef ssize_t        SSIZE;

#define STATIC static
#define CONST const
#define FALSE false
#define TRUE true

// Follows RFC 4122
typedef struct {
	DWORD TimeLow;
	WORD  TimeMid;
	WORD  TimeHighAndVersion;
	BYTE  ClockSeqHighAndReserved;
	BYTE  ClockSeqLow;
	BYTE  Node[6];
} UUID, *LPUUID;

